/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_loop.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvincent <jvincent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 11:06:50 by jvincent          #+#    #+#             */
/*   Updated: 2015/03/01 23:01:53 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

static int	ft_lose(t_grid map)
{
	int		i;
	int		j;

	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			if ((*map)[i][j] == 0)
				return (0);
			if (i < 3 && (*map)[i][j] == (*map)[i + 1][j])
				return (0);
			if (j < 3 && (*map)[i][j] == (*map)[i][j + 1])
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

static int	ft_win(t_grid map)
{
	int		i;
	int		j;

	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			if ((*map)[i][j] == WIN_VALUE)
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}

static void	ft_print_lose_message(char *msg)
{
	clear();
	attron(COLOR_PAIR(6));
	mvprintw(LINES / 2, COLS / 2 - 7, msg);
	attroff(COLOR_PAIR(6));
	mvprintw(LINES / 2 + 1, COLS / 2 - 7, "Type ESC for exit.");
	refresh();
	while (getch() != KEY_ESC)
		;
}

static int	ft_prompt_continue(int *cont)
{
	int		input;

	clear();
	attron(COLOR_PAIR(3));
	mvprintw(LINES / 2, COLS / 2 - 7, "YOU WIN ! CONTINUE ? [y/n]");
	attroff(COLOR_PAIR(3));
	refresh();
	while ((input = getch()) != KEY_N)
	{
		if (input == KEY_Y)
		{
			*cont = 1;
			return (1);
		}
	}
	return (0);
}

void		ft_loop(t_grid map)
{
	int		key;
	int		cont;

	ft_print_window(map);
	key = 0;
	cont = 0;
	while ((key = getch()) != KEY_ESC)
	{
		if (ft_lose(map))
		{
			ft_print_lose_message("BOUHOU YOU LOSE");
			return ;
		}
		if (ft_play(map, key))
		{
			if (!cont && ft_win(map))
			{
				if (!ft_prompt_continue(&cont))
					return ;
			}
			generate_number(map, 0);
		}
		ft_print_window(map);
	}
}
