/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvincent <jvincent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 09:41:23 by jvincent          #+#    #+#             */
/*   Updated: 2015/03/01 22:48:53 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

static size_t	ft_strlen(const char *s)
{
	size_t		i;

	i = 0;
	while (s[i])
		i++;
	return (i);
}

static void		ft_put_error(char *str)
{
	write(2, str, ft_strlen(str));
	write(2, "\n", 1);
}

static void		ft_bzero(void *s, size_t len)
{
	size_t	i;
	char	*ptr;

	if (s)
	{
		ptr = s;
		i = 0;
		while (i < len)
		{
			ptr[i] = 0;
			i++;
		}
	}
}

static void		ft_twenty_forty_eight(void)
{
	t_uint		map[4][4];

	ft_bzero(map, 16 * sizeof(t_uint));
	ft_init_ncurses();
	generate_number((t_grid)map, 2);
	generate_number((t_grid)map, 2);
	ft_loop((t_grid)map);
	endwin();
	return ;
}

int				main(void)
{
	if (!ft_check_win_value(WIN_VALUE))
	{
		ft_put_error("Error: Bad MAX_VALUE.");
		return (EXIT_FAILURE);
	}
	srand(time(NULL));
	ft_twenty_forty_eight();
	return (EXIT_SUCCESS);
}
