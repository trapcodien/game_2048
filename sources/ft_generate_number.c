/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   generate_number.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 18:12:05 by garm              #+#    #+#             */
/*   Updated: 2015/02/28 21:51:12 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

static t_uint	*ft_get_empty_cell(t_grid map)
{
	int		i;
	int		j;
	int		nb;
	t_uint	*empty[16];

	nb = 0;
	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			if ((*map)[i][j] == 0)
			{
				empty[nb] = &((*map)[i][j]);
				nb++;
			}
			j++;
		}
		i++;
	}
	if (nb)
		return (empty[rand() % nb]);
	else
		return (NULL);
}

static t_uint	ft_random_number(int num)
{
	if (num == 0)
		return (((rand() % 4) == 1 ? 4 : 2));
	else
		return (num);
}

void			generate_number(t_grid map, int num)
{
	t_uint		*cell;

	cell = ft_get_empty_cell(map);
	if (cell)
		*cell = ft_random_number(num);
}
