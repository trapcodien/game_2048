/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 14:34:11 by garm              #+#    #+#             */
/*   Updated: 2015/03/01 10:29:59 by jvincent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

static int		move_right(t_grid map, int y)
{
	int			moved;
	int			x;

	moved = 0;
	x = 0;
	if (y < 4)
	{
		while (x < 4 - 1)
		{
			if ((*map)[y][x] > 0 && (*map)[y][x + 1] == 0)
			{
				(*map)[y][x] ^= (*map)[y][x + 1];
				(*map)[y][x + 1] ^= (*map)[y][x];
				(*map)[y][x] ^= (*map)[y][x + 1];
				moved = 1;
			}
			x++;
		}
	}
	return (moved);
}

static int		move_left(t_grid map, int y)
{
	int			moved;
	int			x;

	moved = 0;
	x = 4 - 2;
	if (y < 4)
	{
		while (x >= 0)
		{
			if ((*map)[y][x] == 0 && (*map)[y][x + 1] > 0)
			{
				(*map)[y][x] ^= (*map)[y][x + 1];
				(*map)[y][x + 1] ^= (*map)[y][x];
				(*map)[y][x] ^= (*map)[y][x + 1];
				moved = 1;
			}
			x--;
		}
	}
	return (moved);
}

static int		move_up(t_grid map, int x)
{
	int			moved;
	int			y;

	moved = 0;
	y = 4 - 2;
	if (x < 4)
	{
		while (y >= 0)
		{
			if ((*map)[y][x] == 0 && (*map)[y + 1][x] > 0)
			{
				(*map)[y][x] ^= (*map)[y + 1][x];
				(*map)[y + 1][x] ^= (*map)[y][x];
				(*map)[y][x] ^= (*map)[y + 1][x];
				moved = 1;
			}
			y--;
		}
	}
	return (moved);
}

static int		move_down(t_grid map, int x)
{
	int			moved;
	int			y;

	moved = 0;
	y = 0;
	if (x < 4)
	{
		while (y < 4 - 1)
		{
			if ((*map)[y][x] > 0 && (*map)[y + 1][x] == 0)
			{
				(*map)[y][x] ^= (*map)[y + 1][x];
				(*map)[y + 1][x] ^= (*map)[y][x];
				(*map)[y][x] ^= (*map)[y + 1][x];
				moved = 1;
			}
			y++;
		}
	}
	return (moved);
}

int				ft_move(t_grid map, enum e_direction dir)
{
	t_action	move_func[NB_DIRECTION];
	int			ret;
	int			i;

	move_func[UP] = &move_up;
	move_func[LEFT] = &move_left;
	move_func[RIGHT] = &move_right;
	move_func[DOWN] = &move_down;
	i = 0;
	ret = 0;
	while (i < 4)
	{
		ret += move_func[dir](map, i);
		i++;
	}
	return (ret);
}
