/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   merge.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 15:39:24 by garm              #+#    #+#             */
/*   Updated: 2015/03/01 10:20:55 by jvincent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

static int	merge_down(t_grid map, int x)
{
	int		merged;
	int		y;

	merged = 0;
	y = 4 - 2;
	if (x < 4)
	{
		while (y >= 0)
		{
			if ((*map)[y][x] && (*map)[y][x] == (*map)[y + 1][x])
			{
				(*map)[y][x] = 0;
				(*map)[y + 1][x] *= 2;
				merged = 1;
			}
			y--;
		}
	}
	return (merged);
}

static int	merge_up(t_grid map, int x)
{
	int		merged;
	int		y;

	merged = 0;
	y = 0;
	if (x < 4)
	{
		while (y < 4 - 1)
		{
			if ((*map)[y][x] && (*map)[y][x] == (*map)[y + 1][x])
			{
				(*map)[y + 1][x] = 0;
				(*map)[y][x] *= 2;
				merged = 1;
			}
			y++;
		}
	}
	return (merged);
}

static int	merge_right(t_grid map, int y)
{
	int		merged;
	int		x;

	merged = 0;
	x = 4 - 2;
	if (y < 4)
	{
		while (x >= 0)
		{
			if ((*map)[y][x] && (*map)[y][x] == (*map)[y][x + 1])
			{
				(*map)[y][x] = 0;
				(*map)[y][x + 1] *= 2;
				merged = 1;
			}
			x--;
		}
	}
	return (merged);
}

static int	merge_left(t_grid map, int y)
{
	int		merged;
	int		x;

	merged = 0;
	x = 0;
	if (y < 4)
	{
		while (x < 4 - 1)
		{
			if ((*map)[y][x] && (*map)[y][x] == (*map)[y][x + 1])
			{
				(*map)[y][x] *= 2;
				(*map)[y][x + 1] = 0;
				merged = 1;
			}
			x++;
		}
	}
	return (merged);
}

int			ft_merge(t_grid map, enum e_direction dir)
{
	t_action	merge_func[NB_DIRECTION];
	int			ret;
	int			i;

	merge_func[UP] = &merge_up;
	merge_func[LEFT] = &merge_left;
	merge_func[RIGHT] = &merge_right;
	merge_func[DOWN] = &merge_down;
	i = 0;
	ret = 0;
	while (i < 4)
	{
		ret += merge_func[dir](map, i);
		i++;
	}
	return (ret);
}
