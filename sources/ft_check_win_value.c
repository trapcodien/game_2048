/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_win_value.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 21:50:11 by garm              #+#    #+#             */
/*   Updated: 2015/03/01 10:29:39 by jvincent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

int		ft_check_win_value(int value)
{
	int		nb_onebits;
	int		i;

	nb_onebits = 0;
	i = 0;
	if (value <= 2)
		return (0);
	while (i < 32)
	{
		value = value >> 1;
		if (value % 2)
			nb_onebits++;
		i++;
	}
	return ((nb_onebits == 1) ? (1) : (0));
}
