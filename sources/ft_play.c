/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   play.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 15:46:41 by garm              #+#    #+#             */
/*   Updated: 2015/03/01 09:41:14 by jvincent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

static int	ft_prepare_action(int key, t_direction *dir)
{
	if (key == KEY_UP)
		*dir = UP;
	else if (key == KEY_LEFT)
		*dir = LEFT;
	else if (key == KEY_RIGHT)
		*dir = RIGHT;
	else if (key == KEY_DOWN)
		*dir = DOWN;
	else
		return (0);
	return (1);
}

int			ft_play(t_grid map, int key)
{
	int				ret;
	int				counter;
	t_direction		dir;

	ret = 0;
	counter = 0;
	if (!ft_prepare_action(key, &dir))
		return (ret);
	while ((ret = ft_move(map, dir)))
		counter += ret;
	if ((ret = ft_merge(map, dir)))
		counter += ret;
	while ((ret = ft_move(map, dir)))
		counter += ret;
	return (counter);
}
