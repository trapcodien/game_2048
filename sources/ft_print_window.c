/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_window.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvincent <jvincent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 09:44:00 by jvincent          #+#    #+#             */
/*   Updated: 2015/03/01 20:26:27 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

static void	ft_color(int active, t_uint value, int color_pair)
{
	if (value == 2)
		color_pair = 8;
	if (value == 4)
		color_pair = 2;
	if (value == 8)
		color_pair = 3;
	if (value == 16)
		color_pair = 4;
	if (value == 32)
		color_pair = 5;
	if (value == 64)
		color_pair = 6;
	if (value == 128)
		color_pair = 7;
	if (value == 256)
		color_pair = 8;
	if (value == 512)
		color_pair = 9;
	if (value == 1024)
		color_pair = 10;
	if (active)
		attron(COLOR_PAIR(color_pair));
	else
		attroff(COLOR_PAIR(color_pair));
}

static void	ft_print_numbers(t_grid map)
{
	int		x;
	int		y;
	int		i;
	int		j;

	x = COLS / 4;
	y = LINES / 4;
	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			if ((*map)[i][j] != 0)
			{
				ft_color(1, (*map)[i][j], 11);
				mvprintw(y * i + y / 2 + 1, x * j + x / 2, "%d", (*map)[i][j]);
				ft_color(0, (*map)[i][j], 11);
			}
			j++;
		}
		i++;
	}
}

static void	ft_draw_grid(void)
{
	int		x;
	int		y;

	x = COLS / 2;
	y = LINES / 2;
	mvhline(y, 1, '-', COLS - 2);
	mvhline(y - y / 2, 1, '-', COLS - 2);
	mvhline(y + y / 2, 1, '-', COLS - 2);
	mvvline(1, x, '|', LINES - 2);
	mvvline(1, x - x / 2, '|', LINES - 2);
	mvvline(1, x + x / 2, '|', LINES - 2);
}

int			ft_print_window(t_grid map)
{
	clear();
	if (LINES < 20 || COLS < 50)
	{
		printw("SCREEN TOO SMALL");
		return (1);
	}
	else
	{
		border('|', '|', '-', '-', '+', '+', '+', '+');
		ft_draw_grid();
		ft_print_numbers(map);
	}
	refresh();
	return (0);
}
