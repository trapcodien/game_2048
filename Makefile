#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jvincent <jvincent@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/01/16 10:40:18 by jvincent          #+#    #+#              #
##   Updated: 2015/02/28 21:49:47 by garm             ###   ########.fr       ##
#                                                                              #
#******************************************************************************#

RESET = 		\x1b[0m
RED = 			\x1b[33;01m
GREEN = 		\x1b[32;01m

CC =			gcc

NAME =			game_2048

FLAGS =			-Wpadded -Wall -Wextra -Werror \
				-I./includes \

LDFLAGS = 		-lncurses

SRC_DIR = 		sources/
SRC_FILES =		main.c \
				ft_move.c \
				ft_merge.c \
				ft_play.c \
				ft_check_win_value.c \
				ft_generate_number.c \
				ft_print_window.c \
				ft_init.c \
				ft_loop.c

SRC =			$(addprefix $(SRC_DIR), $(SRC_FILES))
OBJ =			$(SRC:.c=.o)

all: $(NAME)

%.o: %.c includes/game.h
	@$(CC) $(FLAGS) -o $@ -c $<

$(NAME): $(OBJ)
	@$(CC) $(FLAGS) -o $(NAME) $(OBJ) $(LDFLAGS)
	@echo "[$(GREEN)COMPILED$(RESET)] $(NAME)"

clean:
	@rm -f $(OBJ)
	@echo "[$(RED)deleted$(RESET)] Objects"

cleanbin:
	@rm -f $(NAME)

fclean: clean cleanbin
	@echo "[$(RED)deleted$(RESET)] Binary"

re: fclean all

.PHONY: lib clean fclean re all
