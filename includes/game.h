/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 14:25:40 by garm              #+#    #+#             */
/*   Updated: 2015/03/01 23:03:00 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GAME_H
# define GAME_H

# include <unistd.h>
# include <stdlib.h>
# include <time.h>
# include <ncurses.h>

# define KEY_ESC 27
# define KEY_Y 121
# define KEY_N 110

typedef unsigned int	t_uint;
typedef t_uint			t_fuck[4][4];
typedef t_fuck			(*t_grid);
typedef int				(*t_action)(t_grid, int);

enum					e_const
{
	WIN_VALUE = 2048
};

typedef enum			e_direction
{
	UP = 0,
	LEFT,
	RIGHT,
	DOWN,
	NB_DIRECTION
}						t_direction;

/*
** move.c
*/
int						ft_move(t_grid map, enum e_direction d);

/*
** merge.c
*/
int						ft_merge(t_grid map, enum e_direction d);

/*
** play.c
*/
int						ft_play(t_grid map, int key);

/*
** check_win_value.c
*/
int						ft_check_win_value(int value);

/*
** generate_number.c
*/
void					generate_number(t_grid map, int num);

/*
** ft_print_window.c
*/
int						ft_print_window(t_grid map);

/*
** ft_init.c
*/
void					ft_init_ncurses(void);

/*
** ft_loop.c
*/
void					ft_loop(t_grid map);

#endif
